package repositories;

import domain.UserAccount;

import java.util.ArrayList;
import java.util.List;

public class DummyUserAccountRepository implements UserAccountRepository {

    public static List<UserAccount> db = new ArrayList<>();

    @Override
    public void add(UserAccount user){
        db.add(user);
    }

    @Override
    public void delete(UserAccount user){
        db.remove(user);
    }


    }
