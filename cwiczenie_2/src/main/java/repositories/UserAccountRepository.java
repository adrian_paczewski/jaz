package repositories;

import domain.UserAccount;

public interface UserAccountRepository {

    void add(UserAccount user);
    void delete(UserAccount user);
}
