package servlets;

import repositories.DummyUserAccountRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LogInServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();

        DummyUserAccountRepository repo = new DummyUserAccountRepository();

        response.setContentType("text/html");


        for (int i = 0; i < repo.db.size(); i++) {
            if ((repo.db.get(i).getUsername().equals(username)) && (repo.db.get(i).getPassword().equals(password))) {
                session.setAttribute("name", username);
                session.setAttribute("pwd", password);
                response.sendRedirect("UserPage.jsp");
            }

        }
        response.getWriter().println("Błedne haslo lub login");
    }
}
