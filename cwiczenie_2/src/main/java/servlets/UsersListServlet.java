package servlets;

import repositories.DummyUserAccountRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/usersList")
public class UsersListServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        DummyUserAccountRepository repo = new DummyUserAccountRepository();
        response.setContentType("text/html");

        for (int i = 0; i < repo.db.size(); i++) {
            response.getWriter().print("Username : " + repo.db.get(i).getUsername());
            response.getWriter().println("    | is Premium : " + repo.db.get(i).isPremium());
        }

    }
}
