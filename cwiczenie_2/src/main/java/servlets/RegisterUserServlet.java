package servlets;

import domain.UserAccount;
import repositories.DummyUserAccountRepository;
import repositories.UserAccountRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/register")
public class RegisterUserServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        UserAccount newAccount = retrieveDataFromRequest(request);
        UserAccountRepository repository = new DummyUserAccountRepository();

        session.setAttribute("registration", newAccount);

        repository.add(newAccount);
        response.sendRedirect("success.jsp");

    }

    private UserAccount retrieveDataFromRequest(HttpServletRequest request){

        UserAccount account = new UserAccount();
        account.setUsername(request.getParameter("username"));
        account.setPassword(request.getParameter("password"));
        account.setEmail(request.getParameter("email"));

        return account;

    }

}
