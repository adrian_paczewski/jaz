package servlets;

import repositories.DummyUserAccountRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addPremium")
public class AdminPageServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String username = request.getParameter("username");

        DummyUserAccountRepository repo = new DummyUserAccountRepository();

        for (int i = 0; i < repo.db.size(); i++) {
            if ((repo.db.get(i).getUsername().equals(username))) {
                if(!repo.db.get(i).isPremium()) {
                    repo.db.get(i).setPremium(true);
                    response.getWriter().println("Premium has been successfully added");
                } else {
                    repo.db.get(i).setPremium(false);
                    response.getWriter().println("Premium has been successfully removed");

                }
            }
        }
    }
}
