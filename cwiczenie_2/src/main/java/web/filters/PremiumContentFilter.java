package web.filters;

import repositories.DummyUserAccountRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/premiumForm.jsp")
public class PremiumContentFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        DummyUserAccountRepository repo = new DummyUserAccountRepository();

        String username = (String) session.getAttribute("name");

        for (int i = 0; i < repo.db.size(); i++) {
            if ((repo.db.get(i).getUsername().equals(username)) && !username.equalsIgnoreCase("admin")) {
                if (!repo.db.get(i).isPremium()) {
                    httpResponse.setContentType("text/html");
                    httpResponse.getWriter().println("You need Premium to access this content");
                    httpResponse.getWriter().println("<a href=\"UserPage.jsp\">Go back</a>");
                } else {
                    chain.doFilter(request, response);
                }
            }
        }
    }



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void destroy() {

    }
}
