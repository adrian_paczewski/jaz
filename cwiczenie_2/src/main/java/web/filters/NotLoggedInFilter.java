package web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/AdminPage.jsp", "/premiumForm.jsp", "/UserPage.jsp", "/usersList.jsp"})
public class NotLoggedInFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        if(session.getAttribute("name")!=null){
            chain.doFilter(request, response);
        } else {
            httpResponse.setContentType("text/html");
            httpResponse.getWriter().println("You have to log in first!");
            httpResponse.getWriter().println("<a href=\"index.jsp\">Go back</a>");

        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }


    @Override
    public void destroy() {

    }
}
