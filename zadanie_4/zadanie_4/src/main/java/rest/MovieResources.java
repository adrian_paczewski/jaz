package rest;

import domain.Actor;
import domain.Comment;
import domain.Movie;
import domain.services.MovieService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/movies")
public class MovieResources {

    private MovieService db = new MovieService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Movie> getAll()
    {
        return db.getAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {
        Movie result = db.get(id);
        if (result == null) {
            return Response.status(404).build();
        }
        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response Add(Movie movie){
        db.add(movie);
        return Response.ok(movie.getId()).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") int id, Movie m){
        Movie result = db.get(id);
        if(result == null)
            return Response.status(404).build();
        m.setId(id);
        db.update(m);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}/comments")
    public Response delete(@PathParam("id") int id){
        Movie result = db.get(id);
        if(result == null)
            return Response.status(404).build();
        if(result.getComments()==null)
            return null;
        else
            result.getComments().remove(id);
        return Response.ok().build();
    }

    @GET
    @Path("/{movieId}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comment> getComments(@PathParam("movieId") int movieId){
        Movie result = db.get(movieId);
        if(result == null)
            return null;
        if(result.getComments()==null)
            result.setComments(new ArrayList<>());
        return result.getComments();
    }

    @POST
    @Path("/{id}/comments")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addComment(@PathParam("id") int movieId, Comment comment){
        Movie result = db.get(movieId);
        if(result == null)
            return Response.status(404).build();
        if(result.getComments() == null)
            result.setComments(new ArrayList<>());
        result.getComments().add(comment);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/rates")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addRate(@PathParam("id") int movieId, double rate){
        Movie result = db.get(movieId);
        if(result == null)
            return Response.status(404).build();
        result.setRate(rate);
        return Response.ok().build();
    }

    @POST
    @Path("/{id}/actors")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addActor(@PathParam("id") int movieId, Actor actor){
        Movie result = db.get(movieId);
        if(result == null)
            return Response.status(404).build();
        if(result.getActors() == null)
            result.setActors(new ArrayList<>());
        result.getActors().add(actor);
        return Response.ok().build();
    }

    @GET
    @Path("/{movieId}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Actor> getActors(@PathParam("movieId") int movieId){
        Movie result = db.get(movieId);
        if(result.getActors()==null)
            result.setActors(new ArrayList<>());
        return result.getActors();
    }

}
